<!-- Footer -->
<footer class="page-footer font-small primary">

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© 2019 Design & Developed By:
    <a href="https://www.geu.ac.in" target="_blank"> Graphic Era Deemed to be University</a>
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->
<script src="./main.js"></script>
</body>
</html>