<?php
    if (isset($_GET['message'])) {
        $message = $_GET['message'];

        if ($message === "registered") {
            echo '
                <div class="alert alert-success" role="alert">
                 User Registered Successfully 
                </div>
            ';
        }
        elseif ($message === "NoUserFound") {
            echo '
                <div class="alert alert-danger" role="alert">
                 No User Found
                </div>
            ';
        }
        elseif ($message === "IncorrectPassword") {
            echo '
                <div class="alert alert-danger" role="alert">
                 Password Incorrect
                </div>
            ';
        }
    }
?>