<?php
include './src/php/dbh.php';
if (isset($_POST['download-excel-btn'])) {
    $query = mysqli_query($conn, 'SELECT * FROM student'); // Get data from Database from demo table
    $delimiter = ",";
    $filename = "STUDENT.csv"; // Create file name
     
    //create a file pointer
    $f = fopen('php://memory', 'w'); 
     
    //set column headers
    $fields = array('ID', 'First Name', 'Last Name', 'Email', 'Phone No', 'Institute', 'Program', 'First Internship Choice', 'Second Internship Choice', 'Third Internship Choice', 'Program', 'Internship Type', 'State');
    fputcsv($f, $fields, $delimiter);
     
    //output each row of the data, format line as csv and write to file pointer
    while($row = $query->fetch_assoc()){
        $lineData = array($row['id'], $row['first_name'], $row['last_name'], $row['email'], $row['phone'], $row['institution'], $row['program'], $row['first_internship'], $row['second_internship'], $row['third_internship'], $row['program'], $row['type'], $row['state']);
        fputcsv($f, $lineData, $delimiter);
    }
    //move back to beginning of file
    fseek($f, 0);
    //set headers to download file rather than displayed
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="' . $filename . '";');
    //output all remaining data on a file pointer
    fpassthru($f);
}

if (isset($_POST['download-state-btn'])) {
    $state = mysqli_real_escape_string($conn, $_POST['StuSelectState']);
    $query = mysqli_query($conn, "SELECT * FROM student WHERE state = '$state';"); // Get data from Database from demo table
    $delimiter = ",";
    $filename = "STUDENT.csv"; // Create file name
     
    //create a file pointer
    $f = fopen('php://memory', 'w'); 
     
    //set column headers
    $fields = array('ID', 'First Name', 'Last Name', 'Email', 'Phone No', 'Institute', 'Program', 'First Internship Choice', 'Second Internship Choice', 'Third Internship Choice', 'Program', 'Internship Type', 'State');
    fputcsv($f, $fields, $delimiter);
     
    //output each row of the data, format line as csv and write to file pointer
    while($row = $query->fetch_assoc()){
        $lineData = array($row['id'], $row['first_name'], $row['last_name'], $row['email'], $row['phone'], $row['institution'], $row['program'], $row['first_internship'], $row['second_internship'], $row['third_internship'], $row['program'], $row['type'], $row['state']);
        fputcsv($f, $lineData, $delimiter);
    }
    //move back to beginning of file
    fseek($f, 0);
    //set headers to download file rather than displayed
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="' . $filename . '";');
    //output all remaining data on a file pointer
    fpassthru($f);
}

if (isset($_POST['download-program-btn'])) {
    $program = mysqli_real_escape_string($conn, $_POST['StuSelectProgram']);
    $query = mysqli_query($conn, "SELECT * FROM student WHERE program = '$program';"); // Get data from Database from demo table
    $delimiter = ",";
    $filename = "STUDENT.csv"; // Create file name
     
    //create a file pointer
    $f = fopen('php://memory', 'w'); 
     
    //set column headers
    $fields = array('ID', 'First Name', 'Last Name', 'Email', 'Phone No', 'Institute', 'Program', 'First Internship Choice', 'Second Internship Choice', 'Third Internship Choice', 'Program', 'Internship Type', 'State');
    fputcsv($f, $fields, $delimiter);
     
    //output each row of the data, format line as csv and write to file pointer
    while($row = $query->fetch_assoc()){
        $lineData = array($row['id'], $row['first_name'], $row['last_name'], $row['email'], $row['phone'], $row['institution'], $row['program'], $row['first_internship'], $row['second_internship'], $row['third_internship'], $row['program'], $row['type'], $row['state']);
        fputcsv($f, $lineData, $delimiter);
    }
    //move back to beginning of file
    fseek($f, 0);
    //set headers to download file rather than displayed
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="' . $filename . '";');
    //output all remaining data on a file pointer
    fpassthru($f);
}

if (isset($_POST['download-institute-btn'])) {
    $institute = mysqli_real_escape_string($conn, $_POST['myCountry']);
    $query = mysqli_query($conn, "SELECT * FROM student WHERE institution = '$institute';"); // Get data from Database from demo table
   
   
    $delimiter = ",";
    $filename = "STUDENT.csv"; // Create file name
     
    //create a file pointer
    $f = fopen('php://memory', 'w'); 
     
    //set column headers
    $fields = array('ID', 'First Name', 'Last Name', 'Email', 'Phone No', 'Institute', 'Program', 'First Internship Choice', 'Second Internship Choice', 'Third Internship Choice', 'Program', 'Internship Type', 'State');
    fputcsv($f, $fields, $delimiter);
     
    //output each row of the data, format line as csv and write to file pointer
    while($row = $query->fetch_assoc()){
        $lineData = array($row['id'], $row['first_name'], $row['last_name'], $row['email'], $row['phone'], $row['institution'], $row['program'], $row['first_internship'], $row['second_internship'], $row['third_internship'], $row['program'], $row['type'], $row['state']);
        fputcsv($f, $lineData, $delimiter);
    }
    //move back to beginning of file
    fseek($f, 0);
    //set headers to download file rather than displayed
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="' . $filename . '";');
    //output all remaining data on a file pointer
    fpassthru($f);
}

if (isset($_POST['download-internships-btn'])) {
    $internships = mysqli_real_escape_string($conn, $_POST['internshipFields-firstPreference-select']);
    $query = mysqli_query($conn, "SELECT * FROM student WHERE first_internship = '$internships' OR second_internship = '$internships' OR third_internship = '$internships';"); // Get data from Database from demo table
   
   
    $delimiter = ",";
    $filename = "STUDENT.csv"; // Create file name
     
    //create a file pointer
    $f = fopen('php://memory', 'w'); 
     
    //set column headers
    $fields = array('ID', 'First Name', 'Last Name', 'Email', 'Phone No', 'Institute', 'Program', 'First Internship Choice', 'Second Internship Choice', 'Third Internship Choice', 'Program', 'Internship Type', 'State');
    fputcsv($f, $fields, $delimiter);
     
    //output each row of the data, format line as csv and write to file pointer
    while($row = $query->fetch_assoc()){
        $lineData = array($row['id'], $row['first_name'], $row['last_name'], $row['email'], $row['phone'], $row['institution'], $row['program'], $row['first_internship'], $row['second_internship'], $row['third_internship'], $row['program'], $row['type'], $row['state']);
        fputcsv($f, $lineData, $delimiter);
    }
    //move back to beginning of file
    fseek($f, 0);
    //set headers to download file rather than displayed
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="' . $filename . '";');
    //output all remaining data on a file pointer
    fpassthru($f);
}

if (isset($_POST['download-Student-Excel-btn'])) {
    $id = mysqli_real_escape_string($conn, $_POST['id-field']);
    $query = mysqli_query($conn, "SELECT * FROM student WHERE id = '$id';"); // Get data from Database from demo table
   
   
    $delimiter = ",";
    $filename = "STUDENT.csv"; // Create file name
     
    //create a file pointer
    $f = fopen('php://memory', 'w'); 
     
    //set column headers
    $fields = array('ID', 'First Name', 'Last Name', 'Email', 'Phone No', 'Institute', 'Program', 'First Internship Choice', 'Second Internship Choice', 'Third Internship Choice', 'Program', 'Internship Type', 'State');
    fputcsv($f, $fields, $delimiter);
     
    //output each row of the data, format line as csv and write to file pointer
    while($row = $query->fetch_assoc()){
        $lineData = array($row['id'], $row['first_name'], $row['last_name'], $row['email'], $row['phone'], $row['institution'], $row['program'], $row['first_internship'], $row['second_internship'], $row['third_internship'], $row['program'], $row['type'], $row['state']);
        fputcsv($f, $lineData, $delimiter);
    }
    //move back to beginning of file
    fseek($f, 0);
    //set headers to download file rather than displayed
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="' . $filename . '";');
    //output all remaining data on a file pointer
    fpassthru($f);
}

if (isset($_POST['selectState'])) {
    $state = mysqli_real_escape_string($conn, $_POST['state']);
    $sql = "SELECT * FROM student WHERE state = '$state';";
    $result = mysqli_query($conn, $sql);
    $resultChk = mysqli_num_rows($result);
    if ($resultChk < 1) {
        echo '<h3>Total Entries 0</h3>';
    } else {
        echo '<h3>Total Entries '.$resultChk.'</h3>';
    }
}

if (isset($_POST['selectProgram'])) {
    $program = mysqli_real_escape_string($conn, $_POST['program']);
    $sql = "SELECT * FROM student WHERE program = '$program';";
    $result = mysqli_query($conn, $sql);
    $resultChk = mysqli_num_rows($result);
    if ($resultChk < 1) {
        echo '<h3>Total Entries 0</h3>';
    } else {
        echo '<h3>Total Entries '.$resultChk.'</h3>';
    }
}

if (isset($_POST['selectInternship'])) {
    $internship = mysqli_real_escape_string($conn, $_POST['internship']);
    $sql = "SELECT * FROM student WHERE first_internship = '$internship' OR second_internship = '$internship' OR third_internship = '$internship';";
    $result = mysqli_query($conn, $sql);
    $resultChk = mysqli_num_rows($result);
    if ($resultChk < 1) {
        echo '<h3>Total Entries 0</h3>';
    } else {
        echo '<h3>Total Entries '.$resultChk.'</h3>';
    }
}

if (isset($_POST['download-bySelect-btn'])) {
    $internships = mysqli_real_escape_string($conn, $_POST['internshipFields-firstPreference-select']);
    $program = mysqli_real_escape_string($conn, $_POST['StuSelectProgram']);
    $state = mysqli_real_escape_string($conn, $_POST['StuSelectState']);
    
    $query = mysqli_query($conn, "SELECT * FROM student WHERE first_internship = '$internships' AND program = '$program' AND state = '$state';"); // Get data from Database from demo table
   
   
    $delimiter = ",";
    $filename = "STUDENT.csv"; // Create file name
     
    //create a file pointer
    $f = fopen('php://memory', 'w'); 
     
    //set column headers
    $fields = array('ID', 'First Name', 'Last Name', 'Email', 'Phone No', 'Institute', 'Program', 'First Internship Choice', 'Second Internship Choice', 'Third Internship Choice', 'Program', 'Internship Type', 'State');
    fputcsv($f, $fields, $delimiter);
     
    //output each row of the data, format line as csv and write to file pointer
    while($row = $query->fetch_assoc()){
        $lineData = array($row['id'], $row['first_name'], $row['last_name'], $row['email'], $row['phone'], $row['institution'], $row['program'], $row['first_internship'], $row['second_internship'], $row['third_internship'], $row['program'], $row['type'], $row['state']);
        fputcsv($f, $lineData, $delimiter);
    }
    //move back to beginning of file
    fseek($f, 0);
    //set headers to download file rather than displayed
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="' . $filename . '";');
    //output all remaining data on a file pointer
    fpassthru($f);
}
?>