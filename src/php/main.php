<?php
include './dbh.php';
// Register User
if (isset($_POST['register-btn'])) {
    $name = mysqli_real_escape_string($conn, $_POST['name']);
    $email = mysqli_real_escape_string($conn, $_POST['email']);
    $phoneNo = mysqli_real_escape_string($conn, $_POST['phoneNo']);
    $password = mysqli_real_escape_string($conn, $_POST['password']);
    $uid = "STU".uniqid().time();
    $currentTimeinSeconds = time(); 
    $registerOn = date('Y-m-d', $currentTimeinSeconds);
    // Hash Password
    $hash_pwd = password_hash($password, PASSWORD_DEFAULT);
    $sql = "INSERT INTO admin(uid, name, email, phone_no, register_on, password) VALUES('$uid', '$name', '$email', '$phoneNo', '$registerOn', '$hash_pwd');";
    mysqli_query($conn, $sql);
    header("Location: ../../index.php?message=registered");
}

// User Login
if (isset($_POST['userLogin-btn'])) {
    $email = mysqli_real_escape_string($conn, $_POST['email']);
    $password = mysqli_real_escape_string($conn, $_POST['password']);
    $sql = "SELECT * FROM admin WHERE email = '$email';";
    $result = mysqli_query($conn, $sql);
    $resultChk = mysqli_num_rows($result);
    if ($resultChk < 1) {
        header("Location: ../../index.php?message=NoUserFound");
    } else {
        while($row = mysqli_fetch_assoc($result)) {
            $userPassword = $row['password'];
            if (password_verify($password, $userPassword)) {
                header("Location: ../../dashboard.php");
            } else {
                header("Location: ../../index.php?message=IncorrectPassword");
            }
        }
    }
}
?>