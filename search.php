<?php include './includes/header.inc.php'; ?>
<!-- Body Comes Here -->
<div class="container text-center mt-4">
    <img src="images/logo.jpg" alt="AICTE Logo" style="width: 10%;" class="img-fluid" >
</div>
<div class="container mt-3 w-50">
    <div class="ml-3 mt-3 text-center" id="totalbystate">  
        <h3>Total Entries 0</h3>
    </div>

    <div class="card mt-2 mb-3">
        <h5 class="card-header">Select the Category</h5>
        <div class="card-body">
            <form action="database-excel.php" method="POST">
                <div class="form-group">
                <label for="state">Select State</label>
                <select class="custom-select" name="StuSelectState" id="StuSelectState">
                    <option selected>Select State</option>
                    <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                    <option value="Andhra Pradesh">Andhra Pradesh</option>
                    <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                    <option value="Assam">Assam</option>
                    <option value="Bihar">Bihar</option>
                    <option value="Chandigarh">Chandigarh</option>
                    <option value="Chhattisgarh">Chhattisgarh</option>
                    <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                    <option value="Daman and Diu">Daman and Diu</option>
                    <option value="Delhi">Delhi</option>
                    <option value="Goa">Goa</option>
                    <option value="Gujarat">Gujarat</option>
                    <option value="Haryana">Haryana</option>
                    <option value="Himachal Pradesh">Himachal Pradesh</option>
                    <option value="Jammu and Kashmir">Jammu and Kashmir</option>
                    <option value="Jharkhand">Jharkhand</option>
                    <option value="Karnataka">Karnataka</option>
                    <option value="Kerala">Kerala</option>
                    <option value="Lakshadweep">Lakshadweep</option>
                    <option value="Madhya Pradesh">Madhya Pradesh</option>
                    <option value="Maharashtra">Maharashtra</option>
                    <option value="Manipur">Manipur</option>
                    <option value="Meghalaya">Meghalaya</option>
                    <option value="Mizoram">Mizoram</option>
                    <option value="Nagaland">Nagaland</option>
                    <option value="Orissa">Orissa</option>
                    <option value="Pondicherry">Pondicherry</option>
                    <option value="Punjab">Punjab</option>
                    <option value="Rajasthan">Rajasthan</option>
                    <option value="Sikkim">Sikkim</option>
                    <option value="Tamil Nadu">Tamil Nadu</option>
                    <option value="Tripura">Tripura</option>
                    <option value="Uttaranchal">Uttaranchal</option>
                    <option value="Uttar Pradesh">Uttar Pradesh</option>
                    <option value="West Bengal">West Bengal</option>
                </select>
                </div>
                <div class="form-group">
                <label for="state">Select Internship</label>
                <select class="custom-select" id="internshipFields-firstPreference-select" name="internshipFields-firstPreference-select">
                    <option selected>Select Internship</option>
                    <option value="Architecture">Architecture</option>
                    <option value="Interior Design">Interior Design</option>
                    <option value="Commerce">Commerce</option>
                    <option value="Accounts">Accounts</option>
                    <option value="Chartered Accountancy">Chartered Accountancy</option>
                    <option value="Design">Design</option>
                    <option value="Animation">Animation</option>
                    <option value="Fashion Design">Fashion Design</option>
                    <option value="Graphic Design">Graphic Design</option>
                    <option value="Merchandise Design">Merchandise Design</option>
                    <option value="Engineering">Engineering</option>
                    <option value="Aerospace Engineering">Aerospace Engineering</option>
                    <option value="Biotechnology Engineering">Biotechnology Engineering</option>
                    <option value="Chemical Engineering">Chemical Engineering</option>
                    <option value="Civil Engineering">Civil Engineering</option>
                    <option value="Computer Vision">Computer Vision</option>
                    <option value="Electrical Engineering">Electrical Engineering</option>
                    <option value="Electronics Engineering">Electronics Engineering</option>
                    <option value="Energy Science &amp; Engineering">Energy Science &amp; Engineering</option>
                    <option value="Engineering Design">Engineering Design</option>
                    <option value="Engineering Physics">Engineering Physics</option>
                    <option value="Game Development">Game Development</option>
                    <option value="Material Science">Material Science</option>
                    <option value="Mechanical Engineering">Mechanical Engineering</option>
                    <option value="Metallurgical Engineering">Metallurgical Engineering</option>
                    <option value="Mobile App Development">Mobile App Development</option>
                    <option value="Naval Architecture and Ocean Engineeering">Naval Architecture and Ocean Engineeering</option>
                    <option value="Network Engineering">Network Engineering</option>
                    <option value="Petroleum Engineering">Petroleum Engineering</option>
                    <option value="Programming">Programming</option>
                    <option value="Software Development">Software Development</option>
                    <option value="Software Testing">Software Testing</option>
                    <option value="Web Development">Web Development</option>
                    <option value="Hospitality">Hospitality</option>
                    <option value="Hotel Management">Hotel Management</option>
                    <option value="Travel &amp; Tourism">Travel &amp; Tourism</option>
                    <option value="MBA">MBA</option>
                    <option value="Data Entry">Data Entry</option>
                    <option value="Hospitality">Hospitality</option>
                    <option value="Digital Marketing">Digital Marketing</option>
                    <option value="Finance">Finance</option>
                    <option value="General Management">General Management</option>
                    <option value="Human Resources (HR)">Human Resources (HR)</option>
                    <option value="Market/Business Research">Market/Business Research</option>
                    <option value="Marketing">Marketing</option>
                    <option value="Operations">Operations</option>
                    <option value="Sales">Sales</option>
                    <option value="Media">Media</option>
                    <option value="Cinematography">Cinematography</option>
                    <option value="Content Writing">Content Writing</option>
                    <option value="Film Making">Film Making</option>
                    <option value="Journalism">Journalism</option>
                    <option value="Motion Graphics">Motion Graphics</option>
                    <option value="Photography">Photography</option>
                    <option value="Public Relations (PR)">Public Relations (PR)</option>
                    <option value="Social Media Marketing">Social Media Marketing</option>
                    <option value="Video Making/Editing">Video Making/Editing</option>
                    <option value="Videography">Videography</option>
                    <option value="Science">Science</option>
                    <option value="Biology">Biology</option>
                    <option value="Chemistry">Chemistry</option>
                    <option value="Mathematics">Mathematics</option>
                    <option value="Physics">Physics</option>
                    <option value="Statistics">Statistics</option>
                    <option value="Agriculture &amp; Food Engineering">Agriculture &amp; Food Engineering</option>
                    <option value="Campus Ambassador">Campus Ambassador</option>
                    <option value="Company Secretary (CS)">Company Secretary (CS)</option>
                    <option value="Data Science">Data Science</option>
                    <option value="Event Management">Event Management</option>
                    <option value="Humanities">Humanities</option>
                    <option value="Law">Law</option>
                    <option value="Medicine">Medicine</option>
                    <option value="Pharmaceutical">Pharmaceutical</option>
                    <option value="Teaching">Teaching</option>
                    <option value="UI/UX Design">UI/UX Design</option>
                    <option value="Volunteering">Volunteering</option>
                </select>
                </div>
                <div class="form-group">
                    <label for="program">Select Program</label>
                    <select class="custom-select" name="StuSelectProgram" id="StuSelectProgram">
                        <option selected>Select Program</option>
                        <option value="Diploma">Diploma</option>
                        <option value="Undergraduate">Undergraduate</option>
                        <option value="Postgraduate">Postgraduate</option>
                    </select>
                </div>
                <button type="submit" name="download-bySelect-btn" class="btn mt-2 btn-success">
                    <i class="fas fa-file-excel"></i>
                    Download Excel
                </button>
            </form>
        </div>
    </div>

    <div class="card">
        <h5 class="card-header">Download By State</h5>
        <div class="card-body">
            <form action="database-excel.php" method="POST">
                <div class="form-group">
                <select class="custom-select" name="StuSelectState" id="StuSelectState">
                <option selected>Select State</option>
                <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                <option value="Andhra Pradesh">Andhra Pradesh</option>
                <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                <option value="Assam">Assam</option>
                <option value="Bihar">Bihar</option>
                <option value="Chandigarh">Chandigarh</option>
                <option value="Chhattisgarh">Chhattisgarh</option>
                <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                <option value="Daman and Diu">Daman and Diu</option>
                <option value="Delhi">Delhi</option>
                <option value="Goa">Goa</option>
                <option value="Gujarat">Gujarat</option>
                <option value="Haryana">Haryana</option>
                <option value="Himachal Pradesh">Himachal Pradesh</option>
                <option value="Jammu and Kashmir">Jammu and Kashmir</option>
                <option value="Jharkhand">Jharkhand</option>
                <option value="Karnataka">Karnataka</option>
                <option value="Kerala">Kerala</option>
                <option value="Lakshadweep">Lakshadweep</option>
                <option value="Madhya Pradesh">Madhya Pradesh</option>
                <option value="Maharashtra">Maharashtra</option>
                <option value="Manipur">Manipur</option>
                <option value="Meghalaya">Meghalaya</option>
                <option value="Mizoram">Mizoram</option>
                <option value="Nagaland">Nagaland</option>
                <option value="Orissa">Orissa</option>
                <option value="Pondicherry">Pondicherry</option>
                <option value="Punjab">Punjab</option>
                <option value="Rajasthan">Rajasthan</option>
                <option value="Sikkim">Sikkim</option>
                <option value="Tamil Nadu">Tamil Nadu</option>
                <option value="Tripura">Tripura</option>
                <option value="Uttaranchal">Uttaranchal</option>
                <option value="Uttar Pradesh">Uttar Pradesh</option>
                <option value="West Bengal">West Bengal</option>
              </select>
                </div>
                <button type="submit" name="download-state-btn" class="btn btn-success">
                    <i class="fas fa-file-excel"></i>
                    Download Excel
                </button>
            </form>
        </div>
    </div>

    <div class="card mt-2">
        <h5 class="card-header">Download By Program</h5>
        <div class="card-body">
            <form action="database-excel.php" method="POST">
                <div class="form-group">
                <select class="custom-select" name="StuSelectProgram" id="StuSelectProgram">
                    <option selected>Choose...</option>
                    <option value="Diploma">Diploma</option>
                    <option value="Undergraduate">Undergraduate</option>
                    <option value="Postgraduate">Postgraduate</option>
                </select>
                </div>
                <button type="submit" name="download-program-btn" class="btn btn-success">
                    <i class="fas fa-file-excel"></i>
                    Download Excel
                </button>
            </form>
        </div>
    </div>

    <div class="card mt-2 mb-3">
        <h5 class="card-header">Download By Internships</h5>
        <div class="card-body">
            <form action="database-excel.php" method="POST">
                <select class="custom-select" id="internshipFields-firstPreference-select" name="internshipFields-firstPreference-select">
                    <option selected>Choose...</option>
                    <option value="Architecture">Architecture</option>
                    <option value="Interior Design">Interior Design</option>
                    <option value="Commerce">Commerce</option>
                    <option value="Accounts">Accounts</option>
                    <option value="Chartered Accountancy">Chartered Accountancy</option>
                    <option value="Design">Design</option>
                    <option value="Animation">Animation</option>
                    <option value="Fashion Design">Fashion Design</option>
                    <option value="Graphic Design">Graphic Design</option>
                    <option value="Merchandise Design">Merchandise Design</option>
                    <option value="Engineering">Engineering</option>
                    <option value="Aerospace Engineering">Aerospace Engineering</option>
                    <option value="Biotechnology Engineering">Biotechnology Engineering</option>
                    <option value="Chemical Engineering">Chemical Engineering</option>
                    <option value="Civil Engineering">Civil Engineering</option>
                    <option value="Computer Vision">Computer Vision</option>
                    <option value="Electrical Engineering">Electrical Engineering</option>
                    <option value="Electronics Engineering">Electronics Engineering</option>
                    <option value="Energy Science &amp; Engineering">Energy Science &amp; Engineering</option>
                    <option value="Engineering Design">Engineering Design</option>
                    <option value="Engineering Physics">Engineering Physics</option>
                    <option value="Game Development">Game Development</option>
                    <option value="Material Science">Material Science</option>
                    <option value="Mechanical Engineering">Mechanical Engineering</option>
                    <option value="Metallurgical Engineering">Metallurgical Engineering</option>
                    <option value="Mobile App Development">Mobile App Development</option>
                    <option value="Naval Architecture and Ocean Engineeering">Naval Architecture and Ocean Engineeering</option>
                    <option value="Network Engineering">Network Engineering</option>
                    <option value="Petroleum Engineering">Petroleum Engineering</option>
                    <option value="Programming">Programming</option>
                    <option value="Software Development">Software Development</option>
                    <option value="Software Testing">Software Testing</option>
                    <option value="Web Development">Web Development</option>
                    <option value="Hospitality">Hospitality</option>
                    <option value="Hotel Management">Hotel Management</option>
                    <option value="Travel &amp; Tourism">Travel &amp; Tourism</option>
                    <option value="MBA">MBA</option>
                    <option value="Data Entry">Data Entry</option>
                    <option value="Hospitality">Hospitality</option>
                    <option value="Digital Marketing">Digital Marketing</option>
                    <option value="Finance">Finance</option>
                    <option value="General Management">General Management</option>
                    <option value="Human Resources (HR)">Human Resources (HR)</option>
                    <option value="Market/Business Research">Market/Business Research</option>
                    <option value="Marketing">Marketing</option>
                    <option value="Operations">Operations</option>
                    <option value="Sales">Sales</option>
                    <option value="Media">Media</option>
                    <option value="Cinematography">Cinematography</option>
                    <option value="Content Writing">Content Writing</option>
                    <option value="Film Making">Film Making</option>
                    <option value="Journalism">Journalism</option>
                    <option value="Motion Graphics">Motion Graphics</option>
                    <option value="Photography">Photography</option>
                    <option value="Public Relations (PR)">Public Relations (PR)</option>
                    <option value="Social Media Marketing">Social Media Marketing</option>
                    <option value="Video Making/Editing">Video Making/Editing</option>
                    <option value="Videography">Videography</option>
                    <option value="Science">Science</option>
                    <option value="Biology">Biology</option>
                    <option value="Chemistry">Chemistry</option>
                    <option value="Mathematics">Mathematics</option>
                    <option value="Physics">Physics</option>
                    <option value="Statistics">Statistics</option>
                    <option value="Agriculture &amp; Food Engineering">Agriculture &amp; Food Engineering</option>
                    <option value="Campus Ambassador">Campus Ambassador</option>
                    <option value="Company Secretary (CS)">Company Secretary (CS)</option>
                    <option value="Data Science">Data Science</option>
                    <option value="Event Management">Event Management</option>
                    <option value="Humanities">Humanities</option>
                    <option value="Law">Law</option>
                    <option value="Medicine">Medicine</option>
                    <option value="Pharmaceutical">Pharmaceutical</option>
                    <option value="Teaching">Teaching</option>
                    <option value="UI/UX Design">UI/UX Design</option>
                    <option value="Volunteering">Volunteering</option>
              </select>
                <button type="submit" name="download-internships-btn" class="btn mt-2 btn-success">
                    <i class="fas fa-file-excel"></i>
                    Download Excel
                </button>
            </form>
        </div>
    </div>

    

    <div class="card mt-2 mb-3">
        <h5 class="card-header">Download By Institute</h5>
        <div class="card-body">
        <?php include './searchbar-institution.inc.php'; ?>
        </div>
    </div>

</div>
<!-- Body Comes Here -->
<?php include './includes/footer.inc.php'; ?>