<?php include './includes/header.inc.php'; ?>
<!-- Body Comes Here -->
<div class="container text-center mt-4">
    <h3>Internship Portal Admin Dashboard</h3>
    <img src="images/logo.jpg" alt="AICTE Logo" style="width: 15%;" class="img-fluid" >
</div>
<div class="container mt-4">
<div class="row mt-2">
    <div class="col-md-6 m-auto">
      <div class="card card-body">
      <?php include './message.php'; ?>
        <h1 class="text-center mb-3"><i class="fas fa-sign-in-alt"></i>  Login</h1>
        <form action="./src/php/main.php" method="POST">
          <div class="form-group">
            <label for="admission">Email</label>
            <input
              type="email"
              id="email"
              name="email"
              class="form-control"
              placeholder="example@example.com"
            />
          </div>
          <div class="form-group">
            <label for="password">Password</label>
            <input
              type="password"
              id="password"
              name="password"
              class="form-control"
              placeholder="Enter Password"
            />
          </div>
          <button type="submit" name="userLogin-btn" class="btn btn-primary btn-block">Login</button>
        </form>
        <p class="lead mt-4">
          No Account? <a href="register.php">Register</a>
        </p>
      </div>
    </div>
  </div>
</div>
<!-- Body Comes Here -->
<?php include './includes/footer.inc.php'; ?>