<?php include './includes/header.inc.php'; ?>
<!-- Body Comes Here -->
<div class="container text-center mt-4">
    <h3>Internship Portal Admin Dashboard</h3>
</div>
<div class="container mt-3 mr-4 text-center">

<form class="form-inline">
  <div class="form-group mb-2">
    <label for="staticEmail2" class="sr-only">Select State</label>
    <select class="custom-select" name="StuSelectState" id="StuSelectState">
                    <option selected>Select State</option>
                    <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                    <option value="Andhra Pradesh">Andhra Pradesh</option>
                    <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                    <option value="Assam">Assam</option>
                    <option value="Bihar">Bihar</option>
                    <option value="Chandigarh">Chandigarh</option>
                    <option value="Chhattisgarh">Chhattisgarh</option>
                    <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                    <option value="Daman and Diu">Daman and Diu</option>
                    <option value="Delhi">Delhi</option>
                    <option value="Goa">Goa</option>
                    <option value="Gujarat">Gujarat</option>
                    <option value="Haryana">Haryana</option>
                    <option value="Himachal Pradesh">Himachal Pradesh</option>
                    <option value="Jammu and Kashmir">Jammu and Kashmir</option>
                    <option value="Jharkhand">Jharkhand</option>
                    <option value="Karnataka">Karnataka</option>
                    <option value="Kerala">Kerala</option>
                    <option value="Lakshadweep">Lakshadweep</option>
                    <option value="Madhya Pradesh">Madhya Pradesh</option>
                    <option value="Maharashtra">Maharashtra</option>
                    <option value="Manipur">Manipur</option>
                    <option value="Meghalaya">Meghalaya</option>
                    <option value="Mizoram">Mizoram</option>
                    <option value="Nagaland">Nagaland</option>
                    <option value="Orissa">Orissa</option>
                    <option value="Pondicherry">Pondicherry</option>
                    <option value="Punjab">Punjab</option>
                    <option value="Rajasthan">Rajasthan</option>
                    <option value="Sikkim">Sikkim</option>
                    <option value="Tamil Nadu">Tamil Nadu</option>
                    <option value="Tripura">Tripura</option>
                    <option value="Uttaranchal">Uttaranchal</option>
                    <option value="Uttar Pradesh">Uttar Pradesh</option>
                    <option value="West Bengal">West Bengal</option>
                </select>
  </div>
  <div class="form-group ml-2 mb-2">
    <label for="staticEmail2" class="sr-only">Select Internship</label>
    <select class="custom-select" id="internshipFields-firstPreference-select" name="internshipFields-firstPreference-select">
                    <option selected>Select Internship</option>
                    <option value="Architecture">Architecture</option>
                    <option value="Interior Design">Interior Design</option>
                    <option value="Commerce">Commerce</option>
                    <option value="Accounts">Accounts</option>
                    <option value="Chartered Accountancy">Chartered Accountancy</option>
                    <option value="Design">Design</option>
                    <option value="Animation">Animation</option>
                    <option value="Fashion Design">Fashion Design</option>
                    <option value="Graphic Design">Graphic Design</option>
                    <option value="Merchandise Design">Merchandise Design</option>
                    <option value="Engineering">Engineering</option>
                    <option value="Aerospace Engineering">Aerospace Engineering</option>
                    <option value="Biotechnology Engineering">Biotechnology Engineering</option>
                    <option value="Chemical Engineering">Chemical Engineering</option>
                    <option value="Civil Engineering">Civil Engineering</option>
                    <option value="Computer Vision">Computer Vision</option>
                    <option value="Electrical Engineering">Electrical Engineering</option>
                    <option value="Electronics Engineering">Electronics Engineering</option>
                    <option value="Energy Science &amp; Engineering">Energy Science &amp; Engineering</option>
                    <option value="Engineering Design">Engineering Design</option>
                    <option value="Engineering Physics">Engineering Physics</option>
                    <option value="Game Development">Game Development</option>
                    <option value="Material Science">Material Science</option>
                    <option value="Mechanical Engineering">Mechanical Engineering</option>
                    <option value="Metallurgical Engineering">Metallurgical Engineering</option>
                    <option value="Mobile App Development">Mobile App Development</option>
                    <option value="Naval Architecture and Ocean Engineeering">Naval Architecture and Ocean Engineeering</option>
                    <option value="Network Engineering">Network Engineering</option>
                    <option value="Petroleum Engineering">Petroleum Engineering</option>
                    <option value="Programming">Programming</option>
                    <option value="Software Development">Software Development</option>
                    <option value="Software Testing">Software Testing</option>
                    <option value="Web Development">Web Development</option>
                    <option value="Hospitality">Hospitality</option>
                    <option value="Hotel Management">Hotel Management</option>
                    <option value="Travel &amp; Tourism">Travel &amp; Tourism</option>
                    <option value="MBA">MBA</option>
                    <option value="Data Entry">Data Entry</option>
                    <option value="Hospitality">Hospitality</option>
                    <option value="Digital Marketing">Digital Marketing</option>
                    <option value="Finance">Finance</option>
                    <option value="General Management">General Management</option>
                    <option value="Human Resources (HR)">Human Resources (HR)</option>
                    <option value="Market/Business Research">Market/Business Research</option>
                    <option value="Marketing">Marketing</option>
                    <option value="Operations">Operations</option>
                    <option value="Sales">Sales</option>
                    <option value="Media">Media</option>
                    <option value="Cinematography">Cinematography</option>
                    <option value="Content Writing">Content Writing</option>
                    <option value="Film Making">Film Making</option>
                    <option value="Journalism">Journalism</option>
                    <option value="Motion Graphics">Motion Graphics</option>
                    <option value="Photography">Photography</option>
                    <option value="Public Relations (PR)">Public Relations (PR)</option>
                    <option value="Social Media Marketing">Social Media Marketing</option>
                    <option value="Video Making/Editing">Video Making/Editing</option>
                    <option value="Videography">Videography</option>
                    <option value="Science">Science</option>
                    <option value="Biology">Biology</option>
                    <option value="Chemistry">Chemistry</option>
                    <option value="Mathematics">Mathematics</option>
                    <option value="Physics">Physics</option>
                    <option value="Statistics">Statistics</option>
                    <option value="Agriculture &amp; Food Engineering">Agriculture &amp; Food Engineering</option>
                    <option value="Campus Ambassador">Campus Ambassador</option>
                    <option value="Company Secretary (CS)">Company Secretary (CS)</option>
                    <option value="Data Science">Data Science</option>
                    <option value="Event Management">Event Management</option>
                    <option value="Humanities">Humanities</option>
                    <option value="Law">Law</option>
                    <option value="Medicine">Medicine</option>
                    <option value="Pharmaceutical">Pharmaceutical</option>
                    <option value="Teaching">Teaching</option>
                    <option value="UI/UX Design">UI/UX Design</option>
                    <option value="Volunteering">Volunteering</option>
                </select>
  </div>
  <div class="form-group ml-2 mb-2">
    <label for="staticEmail2" class="sr-only">Select Program</label>
    <select class="custom-select" name="StuSelectProgram" id="StuSelectProgram">
        <option selected>Select Program</option>
        <option value="Diploma">Diploma</option>
        <option value="Undergraduate">Undergraduate</option>
        <option value="Postgraduate">Postgraduate</option>
    </select>
  </div>
    <button type="submit" class="btn ml-2 btn-primary mb-2">
        <i class="fas fa-search"></i>
        Search
    </button>
</form>
</div>

<div class="container">
<div style="width: 50%;" class="container float-left mt-2 mb-2">
<canvas id="myChart" width="50" height="50"></canvas>
</div>
<div style="width: 49%;" class="container mt-3 float-right mr-2">
<canvas id="myChart2" width="50" height="50"></canvas>
</div>
</div>
<div class="container">
<div style="width: 50%;" class="container float-left mt-2 mb-2">
<canvas id="myChart3" width="50" height="50"></canvas>
</div>
</div>
<!-- Body Comes Here -->
<script src="chart.js"></script>
<script src="./main.js"></script>
</body>
</html>