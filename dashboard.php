<?php include './includes/header.inc.php'; ?>
<!-- Body Comes Here -->
<div class="container text-center mt-4">
    <img src="images/logo.jpg" alt="AICTE Logo" style="width: 10%;" class="img-fluid" >
</div>
<div class="container mt-2 mb-2 w-75">
<form method="POST">
  <div class="form-group">
    <label for="exampleInputEmail1">Search Student</label>
    <input type="text" class="form-control" id="search" name="search" aria-describedby="emailHelp" placeholder="Search by any field">
  </div>
    <button type="submit" name="searchStudent-btn" class="btn float-right btn-primary">
        <i class="fas fa-search"></i>
        Search
    </button>
    <a href="search.php" class="btn mr-2 float-right btn-info">
        <i class="fas fa-filter"></i>
        Filter Search
    </a>
</form><br><br>

<?php
    include './src/php/dbh.php';
    if (isset($_POST['searchStudent-btn'])) {
        $searchfield = mysqli_real_escape_string($conn, $_POST['search']);
        $sql = "SELECT * FROM student WHERE first_name = '$searchfield' OR last_name = '$searchfield' OR program = '$searchfield' OR state = '$searchfield' OR email = '$searchfield' OR type = '$searchfield';";
        $result = mysqli_query($conn, $sql);
        $resultChk = mysqli_num_rows($result);
        echo '
        <table class="table mt-2 table-striped">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">First</th>
            <th scope="col">Last</th>
            <th scope="col">Institution</th>
            <th scope="col">Program</th>
            <th scope="col">Email</th>
            <th scope="col">Download</th>
          </tr>
        </thead>
        <tbody>
        ';
        while ($row = mysqli_fetch_assoc($result)) {
            echo '
            <form action="database-excel.php" method="POST">
            <input name="id-field" type="hidden" value='.$row['id'].'>
            <tr>
                <th scope="col">'.$row['id'].'</th>
                <th scope="col">'.$row['first_name'].'</th>
                <th scope="col">'.$row['last_name'].'</th>
                <th scope="col">'.$row['institution'].'</th>
                <th scope="col">'.$row['program'].'</th>
                <th scope="col">'.$row['email'].'</th>
                <th scope="col">
                <button class="btn btn-sm btn-success" name="download-Student-Excel-btn">
                    <i class="fas fa-file-excel"></i>
                    Download Excel
                </button>
                </th>
            </tr>
            </form>
            ';
        }
        echo '
        </tbody>
        </table>
        ';
    }
    $sql = "SELECT * FROM student;";
    $result = mysqli_query($conn, $sql);
    $resultChk = mysqli_num_rows($result);
    echo '
    <div class="card mt-1">
        <div class="card-header">
            <h4>Total Number of Students</h4>
        </div>
        <div class="card-body">
            <h5 class="card-title">'.$resultChk.'</h5>
            <p class="card-text">Total '.$resultChk.' has applied for the internship</p>
            <form action="database-excel.php" method="POST">
                <button type="submit" name="download-excel-btn" class="btn btn-success">
                    <i class="fas fa-file-excel"></i>
                    Download Excel
                </button>
            </form>
        </div>
    </div>
    ';
?>
</div>
<!-- Body Comes Here -->
<?php include './includes/footer.inc.php'; ?>