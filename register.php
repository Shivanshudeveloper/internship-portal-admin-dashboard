<?php include './includes/header.inc.php'; ?>
<!-- Body Comes Here -->
<div class="container text-center mt-4">
    <h3>Internship Portal Admin Dashboard</h3>
</div>
<div class="container mt-4">
<div class="row mt-2">
    <div class="col-md-6 m-auto">
      <div class="card card-body">
        <h1 class="text-center mb-3"><i class="fas fa-user-plus"></i> Register</h1>
        <form action="./src/php/main.php" method="POST">
        <div class="form-group">
            <label for="admission">Full Name</label>
            <input
              type="text"
              id="name"
              name="name"
              class="form-control"
              placeholder="Manan Sharma"
            />
          </div>
          <div class="form-group">
            <label for="admission">Email</label>
            <input
              type="email"
              id="email"
              name="email"
              class="form-control"
              placeholder="example@example.com"
            />
          </div>
          <div class="form-group">
            <label for="admission">Phone No.</label>
            <input
              type="text"
              id="phoneNo"
              name="phoneNo"
              class="form-control"
              placeholder="+91"
              value="+91"
            />
          </div>
          <div class="form-group">
            <label for="password">Password</label>
            <input
              type="password"
              id="password"
              name="password"
              class="form-control"
              placeholder="Enter Password"
            />
          </div>
          <button type="submit" name="register-btn" class="btn btn-primary btn-block">Register</button>
        </form>
        <p class="lead mt-4">
          Have an Account? <a href="index.php">Login</a>
        </p>
      </div>
    </div>
  </div>
</div>
<!-- Body Comes Here -->
<?php include './includes/footer.inc.php'; ?>