$("#StuSelectState").click(() => {
    var stateName = $("#StuSelectState").val()
    $("#totalbystate").load("./database-excel.php",{
        state: stateName,
        selectState: true
    })
})

$("#StuSelectProgram").click(() => {
    var program = $("#StuSelectProgram").val()
    $("#totalbystate").load("./database-excel.php",{
        program: program,
        selectProgram: true
    })
})

$("#internshipFields-firstPreference-select").click(() => {
    var internship = $("#internshipFields-firstPreference-select").val()
    $("#totalbystate").load("./database-excel.php",{
        internship: internship,
        selectInternship: true
    })
})
